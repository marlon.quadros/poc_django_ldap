
Liberação do firewall para a portca TCP 389 - Permitir conexão
criação de conta no dominio 
    - 2 contas de usuarios;
        - um conta adm(para login na interface web django) - senha: Dj@ng0@dm@2022
        - uma conta BIND(conta utilizada para acesso o BD do AD) - senha: B1nd@2022
    - 1 Grupo de dominio
        - criar um grupo chamado django-admins
            - OBS: Colocar o user admin dentro do grupo django-admins
Seguem as solicitações:
1 - Foram criadas duas contas de rede:
user: admin-django
senha: Dj@ng0@dm@2022
user: bind
senha: B1nd@2022
2 - Foi criado o grupo de acesso django-admin
3 - O usuário admin-jango foi incluído no grupo django-admin
*4 - IP ou Link de acesso do controlador de dominio: *
ldap://10.100.97.99 ou 10.100.97.98
5 - Dominio do diretorio ativo
dominio: btfinanceira.com.br
6 - Recipientes de autenticação
Acredito que esse item se refira ao container dentro do AD aonde os usuários e grupo estão criados, segue:
btfinanceira.com.br/Grupos/django-admin
btfinanceira.com.br/Topazio/Servicos/admin-django
btfinanceira.com.br/Topazio/Servicos/bind
c-btfinanceira,dc-com,dc-br