dn: ou=people,dc=example,dc=org
objectClass: organizationalUnit
ou: people

dn: ou=groups,dc=example,dc=org
objectClass: organizationalUnit
ou: groups

dn: ou=moregroups,dc=example,dc=org
objectClass: organizationalUnit
ou: moregroups

dn: ou=mirror_groups,dc=example,dc=org
objectClass: organizationalUnit
ou: mirror_groups

dn: uid=alice,ou=people,dc=example,dc=org
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
objectClass: posixAccount
cn: alice
uid: alice
userPassword: password
uidNumber: 1000
gidNumber: 1000
givenName: Alice
mail: alice@example.org
sn: Adams
homeDirectory: /home/alice

dn: uid=bob,ou=people,dc=example,dc=org
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
objectClass: posixAccount
cn: bob
uid: bob
userPassword: password
uidNumber: 1001
gidNumber: 50
givenName: Robert
mail: robert@example.org
sn: Barker
homeDirectory: /home/bob

dn: uid=dreßler,ou=people,dc=example,dc=org
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
objectClass: posixAccount
cn: dreßler
uid: dreßler
userPassword: password
uidNumber: 1002
gidNumber: 50
givenName: Wolfgang
mail: wolfgang@example.org
sn: Dreßler
homeDirectory: /home/dressler

dn: uid=nobody,ou=people,dc=example,dc=org
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
objectClass: posixAccount
cn: nobody
uid: nobody
userPassword: password
uidNumber: 1003
gidNumber: 50
mail: nobody@example.org
sn: nobody
homeDirectory: /home/nobody

dn: uid=nonposix,ou=people,dc=example,dc=org
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
cn: nonposix
uid: nonposix
mail: nonposix@example.org
userPassword: password
sn: nonposix


# posixGroup objects
dn: cn=active_px,ou=groups,dc=example,dc=org
objectClass: posixGroup
cn: active_px
gidNumber: 1000
memberUid: nonposix

dn: cn=staff_px,ou=groups,dc=example,dc=org
objectClass: posixGroup
cn: staff_px
gidNumber: 1001
memberUid: alice
memberUid: nonposix

dn: cn=superuser_px,ou=groups,dc=example,dc=org
objectClass: posixGroup
cn: superuser_px
gidNumber: 1002
memberUid: alice
memberUid: nonposix


# groupOfNames groups
dn: cn=empty_gon,ou=groups,dc=example,dc=org
cn: empty_gon
objectClass: groupOfNames
member:

dn: cn=active,ou=groups,dc=example,dc=org
cn: active
objectClass: groupOfNames
member: uid=alice,ou=people,dc=example,dc=org

dn: cn=active,ou=groups,dc=example,dc=org
cn: active
objectClass: groupOfNames
member: uid=bob,ou=people,dc=example,dc=org

dn: cn=staff,ou=groups,dc=example,dc=org
cn: staff
objectClass: groupOfNames
member: uid=alice,ou=people,dc=example,dc=org

dn: cn=superuser,ou=groups,dc=example,dc=org
cn: superuser
objectClass: groupOfNames
member: uid=alice,ou=people,dc=example,dc=org

dn: cn=other_gon,ou=moregroups,dc=example,dc=org
cn: other_gon
objectClass: groupOfNames
member: uid=bob,ou=people,dc=example,dc=org


# groupOfNames objects for LDAPGroupQuery testing
dn: ou=query_groups,dc=example,dc=org
objectClass: organizationalUnit
ou: query_groups

dn: cn=alice_gon,ou=query_groups,dc=example,dc=org
cn: alice_gon
objectClass: groupOfNames
member: uid=alice,ou=people,dc=example,dc=org

dn: cn=mutual_gon,ou=query_groups,dc=example,dc=org
cn: mutual_gon
objectClass: groupOfNames
member: uid=alice,ou=people,dc=example,dc=org
member: uid=bob,ou=people,dc=example,dc=org

dn: cn=bob_gon,ou=query_groups,dc=example,dc=org
cn: bob_gon
objectClass: groupOfNames
member: uid=bob,ou=people,dc=example,dc=org

dn: cn=dreßler_gon,ou=query_groups,dc=example,dc=org
cn: dreßler_gon
objectClass: groupOfNames
member: uid=dreßler,ou=people,dc=example,dc=org


# groupOfNames objects for selective group mirroring.
dn: cn=mirror1,ou=mirror_groups,dc=example,dc=org
cn: mirror1
objectClass: groupOfNames
member: uid=alice,ou=people,dc=example,dc=org

dn: cn=mirror2,ou=mirror_groups,dc=example,dc=org
cn: mirror2
objectClass: groupOfNames
member:

dn: cn=mirror3,ou=mirror_groups,dc=example,dc=org
cn: mirror3
objectClass: groupOfNames
member: uid=alice,ou=people,dc=example,dc=org

dn: cn=mirror4,ou=mirror_groups,dc=example,dc=org
cn: mirror4
objectClass: groupOfNames
member:


# Nested groups with a circular reference
dn: cn=parent_gon,ou=groups,dc=example,dc=org
cn: parent_gon
objectClass: groupOfNames
member: cn=nested_gon,ou=groups,dc=example,dc=org

dn: CN=nested_gon,ou=groups,dc=example,dc=org
cn: nested_gon
objectClass: groupOfNames
member: uid=alice,ou=people,dc=example,dc=org
member: cn=circular_gon,ou=groups,dc=example,dc=org

dn: cn=circular_gon,ou=groups,dc=example,dc=org
cn: circular_gon
objectClass: groupOfNames
member: cn=parent_gon,ou=groups,dc=example,dc=org