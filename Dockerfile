FROM python:3.9-slim as base

ENV PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PIP_DEFAULT_TIMEOUT=100 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=1 \
    POETRY_VERSION=1.4 \
    DEBIAN_FRONTEND=noninteractive

RUN pip install -U pip \
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get install python3-dev python2.7-dev build-essential \
    libldap2-dev libsasl2-dev slapd ldap-utils tox \
    lcov valgrind nginx -y \
    && pip install "poetry==$POETRY_VERSION"

RUN apt clean && \
    rm -fr /var/lib/apt/lists/*

COPY docker/nginx/nginx.default /etc/nginx/sites-available/default
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

# copy source and install dependencies
RUN mkdir -p /opt/app
RUN mkdir -p /opt/app/django_ldap
COPY requirements.txt docker/server/docker-entrypoint.sh /opt/app/
WORKDIR /opt/app
RUN pip install -r requirements.txt
COPY . /opt/app/django_ldap/
RUN chown -R www-data:www-data /opt/app

# start server
EXPOSE 8010
STOPSIGNAL SIGTERM
CMD ["/opt/app/docker-entrypoint.sh"]