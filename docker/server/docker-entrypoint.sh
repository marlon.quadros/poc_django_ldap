#!/usr/bin/env bash
# start.sh
(cd django_ldap; python manage.py migrate --no-input)
(cd django_ldap; gunicorn django_ldap.wsgi --user www-data --bind 0.0.0.0:8010 --workers 3) &
nginx -g "daemon off;"